def readMCD(mcd) :
  col2index = {}
  index2col = {}

  curId = 0

  for col in mcd.split(' ') :
    col2index[col] = curId
    index2col[curId] = col
    curId += 1

  return col2index, index2col
