#! /usr/bin/env python3

import sys
import argparse

def formatScore(score) :
  return score.replace("%", "")

def formatLang(lang) :
  return lang.split('-')[0]

def formatMetric(metric) :
  return metric

def formatModel(model) :
  return model

if __name__ == "__main__" :
  parser = argparse.ArgumentParser()
  parser.add_argument("resultFile", type=str,
    help="File produced by macaon_data printResults.py")
  parser.add_argument("--caption", default="",
    help="Caption")
  parser.add_argument("--label", default="tab:a",
    help="Label")
  parser.add_argument("--sota", default=False, action="store_true",
    help="Includes sota results.")
  args = parser.parse_args()

  resPerLang = {}
  metrics = ["UPOS", "UAS"]
  models = ["tagger", "eager", "tagparser"]
  regimes = ["sul", "rl", "rlb"]
  
  mustIgnore = True
  
  for line in open(args.resultFile, "r") :
    line = line.strip()
    if len(line) == 0 :
      continue
    if "-----" in line :
      mustIgnore = False
      continue
  
    if mustIgnore :
      continue
  
    lang, metric, score, model = line.split()
    score = formatScore(score)
    lang = formatLang(lang)
    metric = formatMetric(metric)
    model = formatModel(model)

    if lang not in resPerLang :
      resPerLang[lang] = {}
    
    if model not in resPerLang[lang] :
      resPerLang[lang][model] = {}
  
    resPerLang[lang][model][metric] = score


  # Bold for best values
  for lang in resPerLang :
    for model in models :
      for metric in metrics :
        highestValue = None
        for regime in regimes :
          modelName = "%s_incr_%s"%(model, regime)
          if metric not in resPerLang[lang][modelName] :
            continue
          value = resPerLang[lang][modelName][metric]
          if highestValue is None or float(value) > float(highestValue) :
            highestValue = value
        for regime in regimes :
          modelName = "%s_incr_%s"%(model, regime)
          if metric not in resPerLang[lang][modelName] :
            continue
          if resPerLang[lang][modelName][metric] == highestValue :
            resPerLang[lang][modelName][metric] = "\\bf{%s}"%resPerLang[lang][modelName][metric]


  print(r"\begin{table}[htb]")
  print(r"\tabcolsep=0.8mm")
  print(r"\centering")
  print(r"\begin{tabular}{@{}lrrrr@{}} \toprule")
  columns = 2*metrics
  print("& TAGGER & PARSER & \multicolumn{2}{c}{TAGPARSER} \\\\")
  print("Regime & %s \\\\ \midrule"%(" & ".join(columns)))
  for lang in ["French", "English", "German", "Romanian", "Russian", "Arabic", "Chinese"] :
    print("\multicolumn{5}{c}{%s} \\\\"%lang)
    for regime in regimes :
      print("\%s%s{} "%(regime, "t" if regime == "rlb" else ""), end="")
      for model in models :
        modelName = "%s_incr_%s"%(model, regime)
        for metric in metrics :
          if metric in resPerLang[lang][modelName] :
            print("& %s "%resPerLang[lang][modelName][metric], end="")
      print("\\\\")
    if args.sota :
      sota = resPerLang[lang]["sota"]
      print("\sota{} ", end="")
      for metric in 2*metrics :
        if metric in sota :
          print("& %s "%sota[metric], end="")
        else :
          print("& \_ ", end="")
      print("\\\\")
    

  print(r"\bottomrule")
  print(r"\end{tabular}")
  print("\caption{%s}"%args.caption)
  print("\label{tab:%s}"%args.label)
  print(r"\end{table}")

