#! /usr/bin/env python3

import sys 
sys.path.insert(0, "/storage/raid1/homedirs/franck.dary/macaon_data/scripts")
from launchSlurmArray import launchSlurmArray

prefix = "bin/statesNewer/"

names = []
commands = []

for gamma in ["0.8","0.6"] :
  for network in ["base", "semi"] :
    names.append("%s_rl_%s_g%s_%s"%("tagparser","A",gamma,network))
    commands.append('./main.py train rl data/UD_French-GSD_0/train.conllu %s%s --dev data/UD_French-GSD_0/dev.conllu --incr --transitions %s -n 200 --reward %s --silent --gamma %s --network %s'%(prefix, names[-1], "tagparser", "A", gamma, network))
    for reward in ["A", "A2", "E", "G"] :
      for transitions in ["tagparserbt"] :
        for probaBack in ["0.6,04,0.1-0.3,2,0.0", "0.0,10,0.9-1.0,10,0.1", "0.0,100,0.9-1.0,100,0.1"] :
          names.append("%s_rl_%s_g%s_%s_%s"%(transitions,reward,gamma,network,probaBack))
          commands.append('./main.py train rl data/UD_French-GSD_0/train.conllu %s%s --dev data/UD_French-GSD_0/dev.conllu --incr --transitions %s -n 200 --reward %s --silent --gamma %s --network %s --probaStateBack %s'%(prefix, names[-1], transitions, reward, gamma, network, probaBack))

launchSlurmArray(names, commands, "rl", "gpu", 96, 8, 2)

