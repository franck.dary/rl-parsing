#!/bin/bash
RUNS=10000
GRPS=('eager'
      'tagger'
      'tagparser')

computePVal () {
  rm -rf results
  NAME="$1"
  OUTPUT="$1.res"
  METRICS="UAS UPOS"
  if [ "$OUTPUT" = "tagger.res" ]; then
    METRICS="UPOS"
  fi
  if [ "$OUTPUT" = "eager.res" ]; then
    METRICS="UAS"
  fi
  SYSTEMS=$(ls $NAME)
  > $OUTPUT
  for metric in $METRICS; do
    set -x
    set -e
    for sys in $SYSTEMS; do
        mkdir -p results/$sys
        for testset in `ls $NAME/$sys`; do
            udapy read.Conllu zone=gold files=gold/$testset \
                  read.Conllu zone=pred files=$NAME/$sys/$testset ignore_sent_id=1 \
                  util.ResegmentGold \
                  eval.Conll18 print_results=0 print_raw=$metric \
                  > results/$sys/${testset%.conllu}
        done
    done
    echo $metric >> $OUTPUT
    python3 `python3 -c 'import conll18 as x; print(x.__file__)'` -r $RUNS >> $OUTPUT
    echo "" >> $OUTPUT
  done
}

for grp in ${GRPS[@]}; do
  computePVal $grp
done

