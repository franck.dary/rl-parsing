#! /usr/bin/env python3

# EXAMPLE : ./view.py bin/*/*log

import sys
import argparse
import subprocess
from datetime import datetime

if __name__ == "__main__" :
  parser = argparse.ArgumentParser()
  parser.add_argument("files", type=str, nargs="+",
    help="List of train log files, ex. './view.py bin/*/*log'")
  parser.add_argument("--detail", "-d", default=False, action="store_true",
    help="Print saved epoch count details")
  args = parser.parse_args()

  cur = 0
  total = 0
  cmd = 'for f in %s; do tail -1 $f; done'%" ".join(args.files)
  p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
  for line in p.stdout :
    line = line.decode("utf-8").strip().replace(",", " ")
    curEpoch = int(line.split("/")[0].split()[-1])
    totalEpoch = int(line.split("/")[1].split()[0])
    cur += curEpoch
    total += totalEpoch
  print("["+datetime.now().strftime("%d/%m/%y %H:%M:%S")+"]", ":", "%.2f%%"%(100.0*cur/total))

  if not args.detail :
    exit(0)
  
  cmd = 'for f in %s; do echo $f; grep "SAVED" $f | tail -1; echo ""; done'%" ".join(args.files)
  p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
  
  models = {}
  
  model = []
  for line in p.stdout :
    line = line.decode("utf-8").strip()
    if len(line) == 0 :
      epoch = model[1].split(',')[0].split(' ')[-1]
      name = model[0].split('/')[-2]
      category = name.split('_')[0]
      scores = model[1].split(':')[-1].replace(' SAVED', '')
      score = sum([float(val.split('=')[-1]) for val in scores.split()])
      if category not in models :
        models[category] = []
      models[category].append([score, "%s\t%s\t%s"%(epoch, scores, name)])
      model = []
      continue
    model.append(line)
  
  for category in models :
    print(category, ":")
    cat = sorted(models[category])[::-1]
    print("\n".join([m[1] for m in cat]))
